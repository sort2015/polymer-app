var gulp = require('gulp');
var path = require('path');
var join = path.join;
var stylus = require('gulp-stylus');
var root = process.cwd();
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var nodemon = require('gulp-nodemon');

var paths = {
  stylFiles: join(root, "app", "**", "/*.styl"),
  cssFilesDest: join(root, "app")
};

console.log(paths);

gulp.task('compile-stylus', function() {
  gulp.src(paths.stylFiles)
    .pipe(stylus())
    .pipe(gulp.dest(paths.cssFilesDest));
});

gulp.task("watch-stylus", function() {
  gulp.watch(paths.stylFiles, ["compile-stylus"]);
});

gulp.task('styles', ["compile-stylus", "watch-stylus"]);


// we'd need a slight delay to reload browsers
// connected to browser-sync after restarting nodemon
var BROWSER_SYNC_RELOAD_DELAY = 500;

gulp.task('nodemon', ['styles'], function (cb) {
  var called = false;
  return nodemon({
    verbose: false,
    // nodemon our expressjs server
    "exec": "source ~/.nvm/nvm.sh && nvm run unstable server.js",
    env: { 'DEBUG': 'none' },
    // watch core server file(s) that require server restart on change
    watch: ['server.js', 'generateData.js']
  })
    .on('start', function onStart() {
      // ensure start only got called once
      if (!called) { cb(); }
      called = true;
    })
    //.on('restart', function onRestart() {
    //  // reload connected browsers after a slight delay
    //  setTimeout(function reload() {
    //    browserSync.reload({
    //      stream: false
    //    });
    //  }, BROWSER_SYNC_RELOAD_DELAY);
    //});
});

gulp.task('serve', ['nodemon'], function () {
  browserSync({
    open: false,
    port: 3006,
    notify: false,
    logPrefix: 'PGS',
    xsnippetOptions: {
      rule: {
        match: '<span id="browser-sync-binding"></span>',
        fn: function (snippet) {
          return snippet;
        }
      }
    },
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    //server: {
    //  baseDir: ['.tmp', 'app'],
    //  middleware: [  ],
    //  routes: {
    //    '/bower_components': 'bower_components'
    //  }
    //},
    proxy: 'http://localhost:3005'
  });

  gulp.watch(['app/**/*.html'], reload);
  gulp.watch(['app/css/**/*.styl'], ['styles', reload]);
  gulp.watch(['app/elements/**/*.styl'], [reload]);
  gulp.watch(['app/img/**/*'], reload);
});

