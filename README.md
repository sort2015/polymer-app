Sort 2015 Polymer Demonstration App
===================================

# Building During Development
I like to write css in Stylus, but also want to adopt the Polymer way of including css resources in the folder for each element. So, the default gulp task builds stylus files wherever they are in the app directory. You can either run gulp globally, or a safter bet is to run it locally but running the dev-build script identified in package.json:

```
npm run dev-build
```
